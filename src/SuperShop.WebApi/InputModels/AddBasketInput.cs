﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SuperShop.WebApi.InputModels
{
    public class AddBasketInput
    {
        [Required]
        public Guid productId { get; set; }

        [Required]
        public int quantity { get; set; }
    }
}