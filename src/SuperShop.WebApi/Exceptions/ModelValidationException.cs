﻿using System;
using System.Collections.Generic;

namespace SuperShop.WebApi.Exceptions
{
    internal class ModelValidationException : Exception
    {
        public IEnumerable<string> Errors { get; set; }

        public ModelValidationException(IEnumerable<string> errors) : base("Model validation error")
        {
            Errors = errors;
        }
    }
}