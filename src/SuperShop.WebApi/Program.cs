﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using SuperShop.Core.Entities;
using SuperShop.Core.Interfaces;
using SuperShop.Data.Seed;

namespace SuperShop.WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();
            var logger = host.Services.GetRequiredService<ILogger<Program>>();

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var userRepository = services.GetRequiredService<IRepository<User>>();
                var productRepository = services.GetRequiredService<IRepository<Product>>();

                UserSeed.Seed(userRepository);
                ProductSeed.Seed(productRepository);
            }

            var applicationLifetime = host.Services.GetRequiredService<IApplicationLifetime>();
            applicationLifetime.ApplicationStopped.Register(() => logger.LogInformation("Web host stopped"));

            logger.LogInformation("Web host starting");
            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseSerilog((webHostBuilderContext, loggerConfiguration) => loggerConfiguration.ReadFrom.Configuration(webHostBuilderContext.Configuration))
                .UseStartup<Startup>();
    }
}