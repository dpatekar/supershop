﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Logging;
using SuperShop.WebApi.Exceptions;
using SuperShop.WebApi.Responses;
using System;
using System.Threading.Tasks;

namespace SuperShop.WebApi.Middleware
{
    internal class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ObjectResultExecutor _objectResultExecutor;
        private readonly ILogger _logger;

        public ExceptionHandlingMiddleware(RequestDelegate next, ObjectResultExecutor objectResultExecutor, ILogger<ExceptionHandlingMiddleware> logger)
        {
            _next = next;
            _objectResultExecutor = objectResultExecutor;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            ApiErrorResponse errorResponse = null;
            Exception exception = null;
            try
            {
                await _next.Invoke(context);
            }
            catch (ModelValidationException ex)
            {
                errorResponse = new ModelValidationErrorResponse(ex);
                exception = ex;
            }
            catch (Exception ex)
            {
                errorResponse = new ApiErrorResponse(ex);
                exception = ex;
            }
            finally
            {
                if (!context.Response.HasStarted)
                {
                    context.Response.StatusCode = 500;
                    await _objectResultExecutor.ExecuteAsync(new ActionContext() { HttpContext = context }, new ObjectResult(errorResponse));
                }
                if (exception != null)
                {
                    _logger.LogError(exception, "Exception captured by ExceptionHandlingMiddleware");
                }
            }
        }
    }
}