using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using SuperShop.Core.Services;
using System.Diagnostics;
using System.Threading.Tasks;

namespace SuperShop.WebApi.Middleware
{
    internal class LoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        private readonly UserService _userService;

        public LoggingMiddleware(RequestDelegate next, ILogger<LoggingMiddleware> logger, UserService userService)
        {
            _next = next;
            _logger = logger;
            _userService = userService;
        }

        public async Task Invoke(HttpContext context)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();

            _logger.BeginScope("IP", context.Connection.RemoteIpAddress);

            string userId = context.Request.Headers["userId"];            
            if (userId != null)
            {
                _logger.BeginScope("userId", userId);
                var existingUser = _userService.GetUser(userId);
                if (existingUser != null)
                    _logger.BeginScope("userFullName", $"{existingUser.FirstName} {existingUser.LastName}");
            }            

            await _next.Invoke(context);

            timer.Stop();
            _logger.LogInformation("Request execution time: {elapsed} ms", timer.Elapsed.TotalMilliseconds);
        }
    }
}