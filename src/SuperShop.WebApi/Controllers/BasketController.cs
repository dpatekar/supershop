﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SuperShop.Core.Entities;
using SuperShop.Core.Services;
using SuperShop.WebApi.InputModels;
using SuperShop.WebApi.Responses;
using System;
using System.Collections.Generic;

namespace SuperShop.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BasketController : ControllerBase
    {
        private BasketService _basketService;
        private ILogger _logger;

        public BasketController(ProductService productService, BasketService basketService, ILogger<BasketController> logger)
        {
            _basketService = basketService;
            _logger = logger;
        }

        /// <summary>
        /// Get basket items for user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<BasketItem> GetBasketItems([FromHeader]Guid userId)
        {
            _logger.LogInformation("Returning all basket items");
            return _basketService.GetBasket(userId)?.Items;
        }

        /// <summary>
        /// Add Item to basket.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="addBasketInput"></param>
        [HttpPost]
        public IActionResult AddBasketItem([FromHeader]Guid userId, AddBasketInput addBasketInput)
        {
            _logger.LogInformation("Adding basket item {@addBasketInput}", addBasketInput);
            _basketService.AddBasketItem(userId, addBasketInput.productId, addBasketInput.quantity);
            return Ok(new ApiOkResponse());
        }
    }
}