﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SuperShop.Core.Entities;
using SuperShop.Core.Services;
using System.Collections.Generic;

namespace SuperShop.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private ProductService _productService;
        private ILogger _logger;

        public ProductsController(ProductService productService, ILogger<ProductsController> logger)
        {
            _productService = productService;
            _logger = logger;
        }

        /// <summary>
        /// Get all products.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<Product> GetProducts()
        {
            _logger.LogInformation("Returning all products");
            return _productService.GetProducts();
        }
    }
}