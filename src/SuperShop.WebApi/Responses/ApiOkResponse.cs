﻿namespace SuperShop.WebApi.Responses
{
    internal class ApiOkResponse : ApiResponse
    {
        public ApiOkResponse() : base("OK")
        {
        }
    }
}