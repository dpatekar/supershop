﻿using SuperShop.WebApi.Exceptions;
using System.Collections.Generic;

namespace SuperShop.WebApi.Responses
{
    internal class ModelValidationErrorResponse : ApiErrorResponse
    {
        public IEnumerable<string> Errors { get; set; }

        public ModelValidationErrorResponse(ModelValidationException exception) : base(exception)
        {
            this.Errors = exception.Errors;
        }
    }
}