﻿using System;

namespace SuperShop.WebApi.Responses
{
    internal class ApiErrorResponse : ApiResponse
    {
        public string Exception { get; set; }
        public string Source { get; set; }
        public string StackTrace { get; set; }
        public string Message { get; }

        public ApiErrorResponse(Exception exception) : base("ERROR")
        {
            Message = exception.Message;
            Exception = exception.GetType().ToString();
            Source = exception.Source;
            StackTrace = exception.StackTrace;
        }
    }
}