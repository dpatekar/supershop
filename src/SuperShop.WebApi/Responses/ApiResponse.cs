﻿namespace SuperShop.WebApi.Responses
{
    internal abstract class ApiResponse
    {
        public string Status { get; set; }

        public ApiResponse(string status)
        {
            Status = status;
        }
    }
}