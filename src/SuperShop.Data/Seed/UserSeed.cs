﻿using SuperShop.Core.Entities;
using SuperShop.Core.Interfaces;
using System;
using System.Collections.Generic;

namespace SuperShop.Data.Seed
{
    public class UserSeed
    {
        public static void Seed(IRepository<User> repository)
        {
            repository.AddRange(new List<User>(){
                new User("Tom", "Cruise", "t@gmail.com", new Guid("472c7469-48d5-4feb-94aa-3772e906e5a1")),
                new User("Lindsay", "Lohan", "ll@gmail.com", new Guid("472c7469-48d5-4feb-94aa-3772e906e5a2")),
                new User("Alfred", "Pan", "ap@yahoo.com", new Guid("472c7469-48d5-4feb-94aa-3772e906e5a3")),
                new User("Kid", "Rock", "rock@mail.com", new Guid("472c7469-48d5-4feb-94aa-3772e906e5a4"))
            });
        }
    }
}