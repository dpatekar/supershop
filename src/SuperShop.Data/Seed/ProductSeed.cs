﻿using SuperShop.Core.Entities;
using SuperShop.Core.Interfaces;
using System;
using System.Collections.Generic;

namespace SuperShop.Data.Seed
{
    public class ProductSeed
    {
        public static void Seed(IRepository<Product> repository)
        {
            repository.AddRange(new List<Product>(){
                new Product("Chocko cake", 300, new Guid("6395f727-108d-40bf-a5b5-02a29387ebd1")),
                new Product("Super cookies", 200, new Guid("1ed6d030-93ec-4fa4-a8f7-01a058975788")),
                new Product("Lobster", 500, new Guid("31e81931-d679-4c0d-afb0-98aedec715b6")),
                new Product("Burger", 320.55m, new Guid("42660cfd-8649-4559-8717-c0a4e7f4cb86")),
                new Product("Bread", 10, new Guid("c8b37366-1d5f-4479-aee6-e115ebabf1cb")),
                new Product("Popcorn", 50, new Guid("29df467f-b34e-485e-ab02-63219cf842bd")),
                new Product("Steak", 1000, new Guid("25f90a2f-d7b9-4e80-ae87-62a4ab5baa0e"))
            });
        }
    }
}