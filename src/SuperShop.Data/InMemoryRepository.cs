﻿using SuperShop.Core.Entities;
using SuperShop.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SuperShop.Data
{
    public class InMemoryRepository<T> : IRepository<T> where T : BaseEntity
    {
        private List<T> _store;

        public InMemoryRepository()
        {
            _store = new List<T>();
        }

        public void Delete(IComparable id)
        {
            _store.RemoveAll(i => i.Id.Equals(id));
        }

        public T FindById(IComparable id)
        {
            return _store.FirstOrDefault(i => i.Id.Equals(id));
        }

        public T FindBySelector(Func<T, bool> selector)
        {
            return _store.FirstOrDefault(selector);
        }

        public void Add(T item)
        {
            _store.Add(item);
        }

        public IList<T> List()
        {
            return _store;
        }

        public void AddRange(IEnumerable<T> range)
        {
            _store.AddRange(range);
        }

        public void Update(T item)
        {
            var itemForUpdate = _store.First(x => x.Id == item.Id);
            itemForUpdate = item;
        }

        public int Count()
        {
            return _store.Count;
        }
    }
}