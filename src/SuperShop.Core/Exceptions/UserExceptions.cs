﻿using System;

namespace SuperShop.Core.Exceptions
{
    internal class UserNotFoundException : Exception
    {
        public Guid UserId { get; set; }

        public UserNotFoundException(Guid userId) : base($"User {userId} not found")
        {
            UserId = userId;
        }
    }
}