﻿using System;

namespace SuperShop.Core.Exceptions
{
    internal class ProductNotFoundException : Exception
    {
        public Guid ProductId { get; set; }

        public ProductNotFoundException(Guid productId) : base($"Product {productId} not found")
        {
            ProductId = productId;
        }
    }
}