﻿using System;

namespace SuperShop.Core.Exceptions
{
    internal class InvalidQuantityException : Exception
    {
        public int Quantity { get; set; }
        public Guid ProductId { get; set; }

        public InvalidQuantityException(int quantity, Guid productId) : base($"Invalid quantity {quantity} for product {productId}")
        {
            Quantity = quantity;
            ProductId = productId;
        }
    }
}