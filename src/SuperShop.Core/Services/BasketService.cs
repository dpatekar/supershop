﻿using Microsoft.Extensions.Logging;
using SuperShop.Core.Entities;
using SuperShop.Core.Exceptions;
using SuperShop.Core.Interfaces;
using System;

namespace SuperShop.Core.Services
{
    public class BasketService : IService
    {
        private IRepository<Basket> _basketRepository;
        private IRepository<User> _userRepository;
        private IRepository<Product> _productRepository;
        private ILogger _logger;

        public BasketService(IRepository<Basket> basketRepository, IRepository<User> userRepository, IRepository<Product> productRepository, ILogger<BasketService> logger)
        {
            _basketRepository = basketRepository;
            _userRepository = userRepository;
            _productRepository = productRepository;
            _logger = logger;
        }

        public Basket GetBasket(Guid userId)
        {
            var user = _userRepository.FindById(userId);
            if (user == null)
                throw new UserNotFoundException(userId);

            return _basketRepository.FindBySelector(b => b.User.Id == userId);
        }

        public void AddBasketItem(Guid userId, Guid productId, int quantity)
        {
            var user = _userRepository.FindById(userId);
            if (user == null)
                throw new UserNotFoundException(userId);

            var product = _productRepository.FindById(productId);
            if (product == null)
                throw new ProductNotFoundException(productId);

            var basket = _basketRepository.FindBySelector(b => b.User.Id == userId);
            if (basket == null)
            {
                _logger.LogInformation("Basket doesn't exist, creating new basket");
                basket = new Basket(user);
                _basketRepository.Add(basket);
            }

            var updatedBasketItem = basket.AddBasketItem(product, quantity);
            _basketRepository.Update(basket);

            _logger.LogInformation("Updated basket item state {@basketItemState}", updatedBasketItem);
        }
    }
}