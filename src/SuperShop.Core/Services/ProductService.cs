﻿using SuperShop.Core.Entities;
using SuperShop.Core.Interfaces;
using System.Collections.Generic;

namespace SuperShop.Core.Services
{
    public class ProductService : IService
    {
        private IRepository<Product> _productRepository;

        public ProductService(IRepository<Product> productRepository)
        {
            _productRepository = productRepository;
        }

        public IEnumerable<Product> GetProducts()
        {
            return _productRepository.List();
        }

        public void AddProduct(Product product)
        {
            _productRepository.Add(product);
        }
    }
}