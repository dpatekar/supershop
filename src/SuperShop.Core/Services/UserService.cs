﻿using SuperShop.Core.Entities;
using SuperShop.Core.Interfaces;
using System;
using System.Collections.Generic;

namespace SuperShop.Core.Services
{
    public class UserService : IService
    {
        private IRepository<User> _userRepository;

        public UserService(IRepository<User> userRepository)
        {
            _userRepository = userRepository;
        }

        public IEnumerable<User> GetUsers()
        {
            return _userRepository.List();
        }

        public void AddUser(User user)
        {
            _userRepository.Add(user);
        }

        public User GetUser(Guid userId)
        {
            return _userRepository.FindById(userId);
        }

        public User GetUser(string userId)
        {
            return GetUser(new Guid(userId));
        }
    }
}