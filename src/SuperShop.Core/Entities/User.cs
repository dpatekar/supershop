﻿using System;

namespace SuperShop.Core.Entities
{
    public class User : BaseEntity
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Email { get; private set; }

        public User(string firstName, string lastName, string email, Guid? id = null) : base(id)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
        }

        public void UpdateEmail(string email)
        {
            Email = email;
        }
    }
}