﻿using SuperShop.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SuperShop.Core.Entities
{
    public class Basket : BaseEntity
    {
        public User User { get; private set; }
        private readonly List<BasketItem> _items = new List<BasketItem>();
        public IReadOnlyCollection<BasketItem> Items => _items.AsReadOnly();

        public Basket(User user, Guid? id = null) : base(id)
        {
            User = user;
            _items = new List<BasketItem>();
        }

        public BasketItem AddBasketItem(Product product, int quantity)
        {
            var basketItem = _items.FirstOrDefault(i => i.Product == product);
            if (basketItem == null)
            {
                basketItem = new BasketItem(product, quantity);
                _items.Add(basketItem);
            }
            else
            {
                basketItem.UpdateQuantity(quantity);
            }
            return basketItem;
        }

        public BasketItem GetBasketItem(Product product)
        {
            return _items.FirstOrDefault(b => b.Product == product);
        }
    }

    public class BasketItem : BaseEntity
    {
        public Product Product { get; private set; }
        public int quantity { get; private set; }

        public BasketItem(Product product, int quantity, Guid? id = null) : base(id)
        {
            Product = product;
            UpdateQuantity(quantity);
        }

        public void UpdateQuantity(int quantity)
        {
            if (this.quantity + quantity < 0)
            {
                throw new InvalidQuantityException(quantity, Product.Id);
            }
            else
            {
                this.quantity += quantity;
            }
        }
    }
}