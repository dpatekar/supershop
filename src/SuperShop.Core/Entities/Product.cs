﻿using System;

namespace SuperShop.Core.Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; private set; }
        public decimal Price { get; private set; }

        public Product(string name, decimal price, Guid? id = null) : base(id)
        {
            Name = name;
            Price = price;
        }
    }
}