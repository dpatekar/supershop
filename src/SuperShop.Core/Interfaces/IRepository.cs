﻿using SuperShop.Core.Entities;
using System;
using System.Collections.Generic;

namespace SuperShop.Core.Interfaces
{
    public interface IRepository<T> where T : BaseEntity
    {
        void Delete(IComparable id);

        T FindById(IComparable id);

        T FindBySelector(Func<T, bool> selector);

        IList<T> List();

        void Add(T item);

        void AddRange(IEnumerable<T> range);

        void Update(T item);

        int Count();
    }
}