#!/bin/sh
pushd src
docker-compose down
popd

sleep 3

pushd infrastructure/efk
docker-compose down
popd