# Supershop
Supershop is an example .NET Core Web API for demonstrating structured logging in containers.

## Technologies
 - .NET Core 2.2
 - [Serilog 2.8](https://serilog.net/)
 - [Elasticsearch 6.6.1](https://github.com/elastic/elasticsearch)
 - [Kibana 6.6.1](https://github.com/elastic/kibana)
 - [Fluentd 1.3](https://github.com/fluent/fluentd) 

## Start
To start the entire stack run
```sh
> start.bat
```
on Windows or
```sh
$ sh start.sh
```
on Linux.

Supershop Web API Swagger URL: http://localhost:5000/swagger

## Kibana
To start playing with logs, visit http://localhost:5601

Go to "Discover" and create an index pattern like "docker*"

Select "@t" as the log timestamp field.

## Test
To start the script for generating test API requests run
```sh
[test/basket-loading]$ npm install
[test/basket-loading]$ npm start
```
## Requirements
 - docker
 - docker-compose
 - node.js (min v7.6)

## Notes
 ​If you are using Docker on Windows, make sure that you have shared your drive where the source files are with Docker.