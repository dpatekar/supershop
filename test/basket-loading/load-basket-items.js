const axios = require('axios');
const guid = require('uuid/v1');
const randomstring = require("randomstring");

const basketApiUrl = 'http://localhost:5000/api/basket';

var users = require('./users');
var products = require('./products');

var sleepIndex = 1;

loadRegular();
loadWrongUser();
loadWrongProduct();
loadInvalidQuantity();
loadModelBindingError();
loadNegativeQuantity();

changeSleepIndex();

async function changeSleepIndex(){
    while (true){
        sleepIndex = getRandomInt(10, 1500)/1000;
        await sleep(getRandomInt(2000, 15000));
    }
}

async function loadRegular(){
    while (true){
        postBasketItem(users.getRandomUser(), products.getRandomProduct(), getRandomInt(1, 10)); 
        await sleep(getRandomInt(1, 2000)*sleepIndex);
    }
}

async function loadNegativeQuantity(){
    while (true){
        postBasketItem(users.getRandomUser(), products.getRandomProduct(), getRandomInt(7, 15)*-1); 
        await sleep(getRandomInt(5000, 10000)*sleepIndex);
    }
}

async function loadWrongUser(){
    while (true){
        postBasketItem(guid(), products.getRandomProduct(), getRandomInt(1, 20)); 
        await sleep(getRandomInt(5000, 10000));
    }
}

async function loadWrongProduct(){
    while (true){
        postBasketItem(users.getRandomUser(), guid(), getRandomInt(1, 20)); 
        await sleep(getRandomInt(5000, 10000));
    }
}

async function loadInvalidQuantity(){
    while (true){
        postBasketItem(users.getRandomUser(), products.getRandomProduct(), getRandomInt(101, 120)); 
        await sleep(getRandomInt(10000, 20000));
    }
}

async function loadModelBindingError(){
    while (true){
        postBasketItem(guid(), randomstring.generate(getRandomInt(5, 20)), getRandomInt(1, 99) + randomstring.generate(1)); 
        await sleep(getRandomInt(10000, 20000));
    }
}

function postBasketItem(userId, productId, quantity) {
    axios.post(basketApiUrl,
        { productId: productId, quantity: quantity },
        {
            headers: {
                'Content-Type': 'application/json',
                'userId': userId
            }
        }
    ).
        then(function (response) {
            console.log(response.data);
        })
        .catch(function (error) {
            console.log(error.response.data);
        });
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

async function sleep(millis) {
    return new Promise(resolve => setTimeout(resolve, millis));
}