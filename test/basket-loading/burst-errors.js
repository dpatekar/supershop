const axios = require('axios');
const guid = require('uuid/v1');
const randomstring = require("randomstring");

const basketApiUrl = 'http://localhost:5000/api/basket';

var users = require('./users');
var products = require('./products');

loadQuantityError();
loadModelBindingError();
var user = users.getRandomUser();

async function loadQuantityError(){   
    while (true){
        postBasketItem(user, products.getRandomProduct(), getRandomInt(101, 199)*-1); 
        await sleep(getRandomInt(4, 10));
    }
}

async function loadModelBindingError(){   
    while (true){
        postBasketItem(user, products.getRandomProduct(), getRandomInt(1, 99) + randomstring.generate({length:1, charset: 'alphabetic'})); 
        await sleep(getRandomInt(4, 10));
    }
}

function postBasketItem(userId, productId, quantity) {
    axios.post(basketApiUrl,
        { productId: productId, quantity: quantity },
        {
            headers: {
                'Content-Type': 'application/json',
                'userId': userId
            }
        }
    ).
        then(function (response) {
            console.log(response.data);
        })
        .catch(function (error) {
            console.log(error.response.data);
        });
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

async function sleep(millis) {
    return new Promise(resolve => setTimeout(resolve, millis));
}