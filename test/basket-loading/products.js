module.exports = new class Products {
    constructor(){
        this.productsArray = [{productId: "6395f727-108d-40bf-a5b5-02a29387ebd1"},
        {productId: "1ed6d030-93ec-4fa4-a8f7-01a058975788"},
        {productId: "31e81931-d679-4c0d-afb0-98aedec715b6"},
        {productId: "42660cfd-8649-4559-8717-c0a4e7f4cb86"},
        {productId: "c8b37366-1d5f-4479-aee6-e115ebabf1cb"},
        {productId: "29df467f-b34e-485e-ab02-63219cf842bd"},
        {productId: "25f90a2f-d7b9-4e80-ae87-62a4ab5baa0e"}];
     }

     getRandomProduct(){
         return this.productsArray[Math.floor(Math.random()*this.productsArray.length)].productId;
     }
}